Common/default configuration for all Octopy installations.
Configuration for specific devices should be placed in their
own repositories in the parent config folder. The config-loader
will go through all json files in this folder and subfolders,
sort them into alphabetical/numeric order and then load them
and merge them in that order.
